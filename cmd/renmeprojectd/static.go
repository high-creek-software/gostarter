//go:build !release

package main

import (
	"io/fs"
	"os"
)

func LoadStatic() fs.FS {
	// TODO: Rename this import to the proper project name
	return os.DirFS("cmd/renameprojectd/static")
}
