//go:build release

package main

import (
	"embed"
	"io/fs"
	"log"
)

//go:embed static
var static embed.FS

func LoadStatic() fs.FS {
	staticDir, err := fs.Sub(static, "static")
	if err != nil {
		log.Fatalln("Error loading static embed", err)
	}
	return staticDir
}
