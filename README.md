# gostarter

1. Clone the project
2. Rename the project daemon directory: cmd/renameprojectd -> your desired project name
   1. Rename the os.DirFS path in cmd/renameprojectd/static.go

## Build with the embed assets
go build -o renameprojectd -tags=release module.name/cmd/renameprojectd

## Scripts for package.json

"scripts": {
"test": "echo \"Error: no test specified\" && exit 1",
"build": "webpack --mode development --config webpack.config.js",
"watch": "npm run build -- --watch",
"prod": "npm run production",
"production": "NODE_ENV=production webpack --mode production --config webpack.config.js"
}


## Install build dependencies
npm install @babel/core @babel/preset-env @vue/compiler-sfc babel-loader css-loader file-loader mini-css-extract-plugin postcss-loader sass sass-loader vue-loader webpack webpack-cli --save-dev

## Install runtime depenedencies
npm install @fortawesome/fontawesome-free axios bulma vue
