package internal

import "io/fs"

type Application struct {
	static fs.FS
}

func NewApplication(static fs.FS) *Application {
	app := &Application{static: static}
	app.init()

	return app
}

func (a *Application) init() {

}

func (a *Application) Start() {
	
}